def vowel_swapper(string):
    # ==============
    # Your code here
    new_string = ""

    a1 = 'a'
    a2 = 'e'
    a3 = 'i'
    a4 = 'o'
    a5 = 'O'
    a6 = 'u'
    for c in string:
        if c.lower() == a1:
          new_string += '4'
        elif c.lower() == a2:
          new_string += '3'
        elif c.lower() == a3:
          new_string +='!'
        elif c == a4:
          new_string += 'ooo'
        elif c == a5:
          new_string += 'OOO'
        elif c.lower() == a6:
          new_string += '|_|'
        else:
          new_string +=c
        
    return new_string


    # ==============

print(vowel_swapper("aA eE iI oO uU")) # Should print "44 33 !! ooo000 |_||_|" to the console
print(vowel_swapper("Hello World")) # Should print "H3llooo Wooorld" to the console 
print(vowel_swapper("Everything's Available")) # Should print "3v3ryth!ng's 4v4!l4bl3" to the console
